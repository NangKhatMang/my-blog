<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    //return view('welcome');
    return 'Hello Laravel';
});*/



//Authentication scaffolding
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// blog home
Route::get('/','PostController@index');

// Blog Post
Route::get('post/{post}','PostController@show');

//Upload Post
Route::get('/upload','PostController@create');
Route::post('/upload','PostController@store');
Route::get('/post/delete/{post}','PostController@destroy');
Route::get('/post/edit/{post}','PostController@edit');
Route::post('/post/edit','PostController@update');

//comment
Route::post('/comment','CommentController@store');

//Category
Route::get('/category','CategoryController@index');
Route::get('/createcategory','CategoryController@create');
Route::post('/storecategory','CategoryController@store');
Route::get('/category/delete/{category}','CategoryController@destroy');
Route::get('/category/edit/{category}','CategoryController@edit');
Route::post('/category/edit','CategoryController@update');

//For Admin Table Auth
Route::get('admin-login','Auth\AdminLoginController@showLoginForm');
Route::post('admin-login','Auth\AdminLoginController@login');

Route::get('/backend','AdminController@index');

