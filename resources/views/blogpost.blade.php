@extends('layouts.template')

@section('content')

<div class="col-md-8">

          <!-- Title -->
          <h1 class="mt-4">{{$post->title}}</h1>

          <!-- Author -->
          <p class="lead">
            by
            <a href="#">{{$post->user->name}}</a>
            
            @if(Auth::check() && ($post->user_id == auth()->id()))
            <a href="/post/edit/{{$post->id}}">Edit</a>
            <a href="/post/delete/{{$post->id}}">Delete</a>
            @endif
          </p>

          <hr>

          <!-- Date/Time -->
          <p>Posted on {{$post->created_at->toDayDateTimeString()}}</p>

          <hr>

          <!-- Preview Image -->
          <img class="img-fluid rounded" src="{{$post->photo}}" alt="">

          <hr>

          <!-- Post Content -->
          {{$post->body}}

         

          <hr>

          <!-- Comments Form -->
          @if(!Auth::check())

            <div class="alert alert-danger">
              You must login First!
            </div>
          @endif

          <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
              <form method="POST" action="/comment">
                @csrf
                <input type="hidden" name="postid" value="{{$post->id}}">
                <div class="form-group">
                  <textarea class="form-control" name="comment" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>

          <!-- Single Comment -->

          @foreach($post->comments as $comment)

          <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
            <div class="media-body">
              <h5 class="mt-0">{{$comment->user->name}} </h5>
              <span>{{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</span></br>

              {{$comment->body}}
            </div>
          </div>

          @endforeach

          <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
            <div class="media-body">
              <h5 class="mt-0">Commenter Name</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
            </div>
          </div>



          

        </div>
@endsection