@extends('layouts.template')

@section('content')

<div class="col-md-8">
	@if(count($errors))
	<dir class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</dir>
	@endif



<form method="post" action="/post/edit" enctype="multipart/form-data" class="my-3">
	@csrf

	<input type="hidden" name="editid" value="{{$post->id}}">
	<div class="form-group">
		<label>Post title:</label>
		<input type="text" name="title" class="form-control" value="{{$post->title}}"> 
		
		<label> Photo:</label>
		<input type="file" name="photo" class="form-control-file" value="">
	</div>
	<div class="form-group">
		<label>Category:</label>
		<select class="form-control" name="category">
			

			@foreach($categories as $category)
				<!-- $id = {{$category->id}};
				$name = {{$category->category_name}};  -->
				
<option value="{{$category->id}}" @if($category->id==$post->category_id) {{'selected'}} @endif >{{$category->category_name}} 
				</option>
			@endforeach
			
		</select>
        

	</div>

	<div class="form-group">
		<label>Post Body:</label>
		<textarea name="body" class="form-control" id="summernote">{{$post->body}}</textarea>
		
	</div>

	<div class="form-group">
		<input type="submit" name="bunsubmit" class="btn btn-primary" value="Edit">
		
	</div>

	
</form>

</div>

@endsection