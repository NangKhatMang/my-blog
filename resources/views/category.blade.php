@extends('layouts.template')

@section('content')

		<div class="col-md-8">
			<br>
			
			
			<a href="/createcategory" class="btn btn-outline-primary float-right mb-2" >Add New Category</a>
				
			
			 <table class="table table-striped table-bordered ">
			 	<thead class="table-secondary">
			 		<tr>
			 			<th>No</th>
			 			<th>Category Name</th>
			 			<th>Action</th>
			 		</tr>
			 	</thead>
			 	<tbody>
			 		 @foreach($categories as $category)
			 		 <tr>
			 		 	<td>{{$category->id}}</td>
			 		 	<td>{{$category->category_name}}</td>
			 		 	<td><a href="/category/edit/{{$category->id}}" class="btn btn-warning">Edit</a>
			 		 		<a href="/category/delete/{{$category->id}}" class="btn btn-danger">Delete</a>
			 		 	</td>
			 		 	
			 		 </tr>
			 		 @endforeach

			 	</tbody>
			 	
			 </table>

		</div>

@endsection