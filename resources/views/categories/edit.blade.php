@extends('layouts.template')

@section('content')

	<div class="col-md-8">

		@if(count($errors))
			<dir class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
					<li>{{$error}}</li>
					@endforeach
				</ul>
			</dir>
		@endif		

		<form method="POST" action="/category/edit" class="mt-3">
			@csrf

			<input type="hidden" name="editid" value="{{$category->id}}">
			<div class="form-group">
				<label>Enter Category Name</label>
				<input type="text" name="catename" class="form-control" value="{{$category->category_name}}">				
			</div>
			<div class="form-group">
				<input type="submit" name="btnsubmit" class="btn btn-primary" value="Update">
		
			</div>
			
		</form>
	</div>

@endsection